import { filter, each, map, mapValues, assign } from 'lodash';
import * as fs from 'fs';
import * as path from 'path';
import { parse } from 'dotenv';
import { info } from '@mvf/logger';

const rule = /(\${([A-Z_]*)})|(\$([A-Z_]*))/;

const exist = (file: string): boolean => {
  return fs.existsSync(file);
};

const evaluateVariables = (value: string) => {
  const matches = rule.exec(value);

  let variable = value;
  if (matches) {
    const groupA = matches[2];
    const groupB = matches[4];

    if (groupA) {
      const env = process.env[groupA];
      variable = env || '';
    } else if (groupB) {
      const env = process.env[groupB];
      variable = env || '';
    }
  }

  return variable;
};

const loadEnvironmentFile = (file: string): Record<string, string> => {
  const dotenvPath = path.resolve(process.cwd(), file);
  const fileBuffer = fs.readFileSync(dotenvPath);
  const variables = parse(fileBuffer);

  return mapValues(variables, evaluateVariables);
};

const applyEnvironmentVariable = (value: string, key: string): void => {
  if (process.env[key]) {
    info(`Variable '${key}' already exists in the environment`);
  } else {
    process.env[key] = value;
  }
};

const overload = (...files: string[]): void => {
  const variables = map(filter(files, exist), loadEnvironmentFile);
  each(assign({}, ...variables), applyEnvironmentVariable);
};

export default overload;
