import mock from 'mock-fs';
import * as Logger from '@mvf/logger/client';
import overload from '../overload';
import fileContentsMock from '../__mocks__/overload.mock';

describe('exec', () => {
  const spyLog = jest.spyOn(Logger, 'info');

  beforeEach(() => {
    mock(fileContentsMock);
  });

  afterEach(() => {
    delete process.env.MOCK_VARIABLE_A;
    delete process.env.MOCK_VARIABLE_B;
    spyLog.mockClear();
    mock.restore();
  });

  it('should load variables from file into process.env', () => {
    overload('/mockedEnvFileA');
    expect(process.env.MOCK_VARIABLE_A).toEqual('A');
  });

  it('should load variables from multiple files into process.env', () => {
    overload('/mockedEnvFileA', '/mockedEnvFileB');
    expect(process.env.MOCK_VARIABLE_B).toEqual('B');
  });

  it('should overwrite variables if they exist in multiple files', () => {
    overload('/mockedEnvFileA', '/mockedEnvFileC');
    expect(process.env.MOCK_VARIABLE_A).toEqual('another A');
  });

  // eslint-disable-next-line no-template-curly-in-string
  it('should load value from environment if value is another variable ${TEST}', () => {
    process.env.TEST = 'A';
    overload('/mockedEnvFileA', '/mockedEnvFileD');
    expect(process.env.MOCK_VARIABLE_A).toEqual('A');
  });

  it('should load value from environment if value is another variable $TEST', () => {
    process.env.TEST = 'B';
    overload('/mockedEnvFileA', '/mockedEnvFileE');
    expect(process.env.MOCK_VARIABLE_A).toEqual('B');
  });

  it('should not change variables if files do not exist in file system', () => {
    overload('/mockedEnvFileA');
    const mockVariableA = process.env.MOCK_VARIABLE_A;
    overload('/mockedEnvFileF');
    expect(mockVariableA).toEqual(process.env.MOCK_VARIABLE_A);
  });

  it('should not overwrite variables already in the environment', () => {
    process.env.MOCK_VARIABLE_A = 'persistent';
    spyLog.mockImplementation((() => {}) as never);
    overload('/mockedEnvFileA', '/mockedEnvFileF');
    expect(process.env.MOCK_VARIABLE_A).toEqual('persistent');
  });

  it('should not overwrite variables already in the environment', () => {
    process.env.MOCK_VARIABLE_A = 'persistent';
    overload('/mockedEnvFileA', '/mockedEnvFileF');
    expect(spyLog).toHaveBeenCalledWith(
      expect.stringContaining("'MOCK_VARIABLE_A' already exists"),
    );
  });
});
