const fileContentsMock: Record<string, string> = {
  '/mockedEnvFileA': 'MOCK_VARIABLE_A=A',
  '/mockedEnvFileB': 'MOCK_VARIABLE_B=B',
  '/mockedEnvFileC': 'MOCK_VARIABLE_A=another A',
  // eslint-disable-next-line no-template-curly-in-string
  '/mockedEnvFileD': 'MOCK_VARIABLE_A=${TEST}',
  '/mockedEnvFileE': 'MOCK_VARIABLE_A=$TEST',
};

export default fileContentsMock;
