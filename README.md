# MVF Env

## Usage

Provides a consistent way handle environment variables in javascript applications.

### To install the package

Run `npm install @mvf/env`

### How to use
TODO

## Contributing

### Setup

- Run `make` to build the container
- Run `make shell` to enter the container
- Run `npm install` to install dependencies

Refer to package.json for commands

### After merging

After you have merged a PR to master, you need to rebuild and publish your changes.

1. Checkout master `git checkout master && git pull`
2. Use one of the following make publish commands to publish changes:
   - `make publish kind=patch` - Use this if your change is a bug fix and is backwards compatible.
   - `make publish kind=minor` - Use this if your change adds new functionality and is backwards compatible.
   - `make publish kind=major` - Use this if your change is not backwards compatible.
